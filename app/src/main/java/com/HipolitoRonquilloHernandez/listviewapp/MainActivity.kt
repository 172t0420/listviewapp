package com.HipolitoRonquilloHernandez.listviewapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Manzana")
        frutas.add("Platano")
        frutas.add("Guayaba")
        frutas.add("Kiwi")
        frutas.add("Limon")
        frutas.add("Uva")
        frutas.add("Naranja")
        frutas.add("Fresa")
        frutas.add("Mango")
        frutas.add("Sandia")

        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        lista.adapter= adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(this,frutas.get(i), Toast.LENGTH_SHORT).show()
            if(i==0){
                val intent=Intent(this,ManzanaActivity::class.java)
                startActivity(intent)
            }
            else  if (i==1){
                val intent=Intent(this,PlatanoActivity::class.java)
                startActivity(intent)
            }
            else  if (i==2){
                val intent=Intent(this,GuayabaActivity::class.java)
                startActivity(intent)
            }
            else  if (i==3){
                val intent=Intent(this,KiwiActivity::class.java)
                startActivity(intent)
            }
            else  if (i==4){
                val intent=Intent(this,LimonActivity::class.java)
                startActivity(intent)
            }
            else  if (i==5){
                val intent=Intent(this,UvaActivity::class.java)
                startActivity(intent)
            }
            else  if (i==6){
                val intent=Intent(this,NaranjaActivity::class.java)
                startActivity(intent)
            }
            else  if (i==7){
                val intent=Intent(this,FresaActivity::class.java)
                startActivity(intent)
            }
            else  if (i==8){
                val intent=Intent(this,MangoActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent=Intent(this,SandiaActivity::class.java)
                startActivity(intent)
            }
        }
    }
}